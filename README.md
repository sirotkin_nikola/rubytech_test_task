1. ### Установите virtualenv командой ``pip install virtualenv``
2. ### Перейдите в директорию проекта.
3. ### Создайте виртуальное окружение командой ``virtualenv venv``
4. ### Активируйте виртуальное окружение ``source venv/bin/activate``
5. ### Перейдите в директорию с файлом requirements.txt
6. ### Установите зависимости ``pip install -r requirements.txt``
7. ### Перейдите в директорию с файло main.py
8. ### Запустите файл main.py командой ``python3 main.py``