import re


class TextAnalyzer:
    def __init__(self, text):
        self.text = text

    def longest_word(self):
        words = re.findall(r'\b\w+\b', self.text)
        if not words:
            print("В тексте нет слов.")
            return None
        longest_word = max(words, key=len)
        print("Самое длинное слово:", longest_word)

    def most_common_word(self):
        words = re.findall(r'\b\w+\b', self.text)
        if not words:
            print("В тексте нет слов.")
            return None
        word_counts = {}
        for word in words:
            word_counts[word] = word_counts.get(word, 0) + 1
        most_common_word = max(word_counts, key=word_counts.get)
        print("Самое часто встречающееся слово:", most_common_word)

    def count_special_characters(self):
        special_characters = re.findall(r'[^\w\s]', self.text)
        if not special_characters:
            print("В тексте нет спецсимволов.")
            return None
        num_special_characters = len(special_characters)
        print("Количество спецсимволов:", num_special_characters)

    def palindromes(self):
        words = re.findall(r'\b\w+\b', self.text)
        palindromes = [word for word in words if word.lower() == word.lower()[::-1] and len(word) > 1]
        if not palindromes:
            print("В тексте нет палиндромов.")
            return None
        print("Палиндромы:", ", ".join(palindromes))
