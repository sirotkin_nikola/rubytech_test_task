import time
import functools
import re


def measure_time(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        start_time = time.time()
        result = func(*args, **kwargs)
        end_time = time.time()
        print(f"Время выполнения функции {func.__name__}: {round(end_time - start_time, 10)} секунд")
        return result
    return wrapper


class TextAnalyzerTimer:
    def __init__(self, text):
        self.text = text

    @measure_time
    def longest_word(self):
        words = re.findall(r'\b\w+\b', self.text)
        if not words:
            print("В тексте нет слов.")
            return None
        longest_word = max(words, key=len)
        print("Самое длинное слово:", longest_word)

    @measure_time
    def most_common_word(self):
        words = re.findall(r'\b\w+\b', self.text)
        if not words:
            print("В тексте нет слов.")
            return None
        word_counts = {}
        for word in words:
            word_counts[word] = word_counts.get(word, 0) + 1
        most_common_word = max(word_counts, key=word_counts.get)
        print("Самое часто встречающееся слово:", most_common_word)

    @measure_time
    def count_special_characters(self):
        special_characters = re.findall(r'[^\w\s]', self.text)
        if not special_characters:
            print("В тексте нет спецсимволов.")
            return None
        num_special_characters = len(special_characters)
        print("Количество спецсимволов:", num_special_characters)

    @measure_time
    def palindromes(self):
        words = re.findall(r'\b\w+\b', self.text)
        palindromes = [word for word in words if word.lower() == word.lower()[::-1] and len(word) > 1]
        if not palindromes:
            print("В тексте нет палиндромов.")
            return None
        print("Палиндромы в тексте:", ", ".join(palindromes))

