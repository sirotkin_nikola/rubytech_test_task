import asyncio
from a import extract_github_project_names
from b import two_lists_to_dict
from c import str_modificate
from d import main_function
from e import TextAnalyzer
from f import TextAnalyzerTimer

link_list = [
    "https://github.com/wandb/openui",
    "https://github.com/jasonppy/VoiceCraft.git",
    "https://github.com/OpenBMB/ChatDev.git",
    "https://test_error_link.com"
]


test_keys = ['a', 'c', 'b', 'd']
test_values = ['world', 2, 'peace']

str_int_list = ['word_1', 'word_2', 3, 'word_3', 4, 5]

test_text = "частое ,слово, слово, частое частое радар, привет / % параллелепипед"
analyzer = TextAnalyzer(test_text)
analyzer_timer = TextAnalyzerTimer(test_text)

if __name__ == '__main__':
    print('------Result task A------')
    extract_github_project_names(link_list)
    print('------Result task B------')
    two_lists_to_dict(test_keys, test_values)
    print('------Result task C------')
    str_modificate(str_int_list)
    print('------Result task D------')
    asyncio.run(main_function())
    print('------Result task E------')
    analyzer.longest_word()
    analyzer.most_common_word()
    analyzer.count_special_characters()
    analyzer.palindromes()
    print('------Result task F------')
    analyzer_timer.longest_word()
    analyzer_timer.most_common_word()
    analyzer_timer.count_special_characters()
    analyzer_timer.palindromes()
