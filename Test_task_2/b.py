def two_lists_to_dict(keys, values):
    if not keys or not values:
        print("Списки не должны быть пустыми")
    try:
        result_dict = dict(zip(keys, values))
        result_dict = dict(sorted(result_dict.items()))
        print(result_dict)
    except TypeError as er:
        print(f'Ошибка ввода данных: {er}')

