import requests
import re


def extract_github_project_names(links):
    github_url_pattern = r'https://github\.com/[\w-]+/([\w-]+)(?:\.git)?/?$'
    for link in links:
        if re.match(github_url_pattern, link):
            match = re.search(github_url_pattern, link)
            if match:
                print(f'Название проекта: {match.group(1)}')
            else:
                print("Название проекта не не соответствует формату:", link)
        else:
            print("Ссылка не соответствует формату GitHub проекта:", link)

