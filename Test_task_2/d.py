import asyncio
import aiohttp
import time


async def fetch_url(session, url):
    async with session.get(url) as response:
        return await response.text()


async def make_requests():
    url = "http://httpbin.org/delay/3"
    tasks = []
    async with aiohttp.ClientSession() as session:
        for _ in range(100):
            task = asyncio.create_task(fetch_url(session, url))
            tasks.append(task)
        responses = await asyncio.gather(*tasks)
        return responses


async def time_measuring():
    start_time = time.time()
    await make_requests()
    end_time = time.time()
    return end_time - start_time


async def main_function():
    execution_time = await time_measuring()
    print(f"Время выполнения запросов: {round(execution_time,  2)} seconds")



